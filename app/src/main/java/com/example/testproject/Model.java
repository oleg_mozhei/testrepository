package com.example.testproject;

import java.util.Calendar;

public class Model {
    private DataBase db;

    //инфа для графика на 2-е суток (с 10-00 включительно)

    public Model(DataBase db){
        this.db = db;
    }

    public int[] getDataFromDay(Calendar date){
        //определяю текущую дату
        int current_day = 7; //просто случайное число, метод возвращает данные не конкретного дня, а генерирует случайные
        return db.getData(current_day);
    }
}
