package com.example.testproject;

import java.util.Random;

public class DataBase {
    DataBase(){
        //реализую конструктор
    }

    int[] getData(int day){
        //реализую метод, который возвращат массив данных в формате int;

        //для примера верну массив интов от 3 до 20, за 48 часов (2-е суток)
        final Random random = new Random();
        int[] result = new int[48];
        for (int i = 0; i < 48; i++){
            result[i] = (random.nextInt(18) + 3);
        }
        return result;
    }
}
