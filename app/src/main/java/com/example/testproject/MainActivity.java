package com.example.testproject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    ImageButton toolbar_button, button_back, button_forward;
    DrawerLayout drawer;
    TextView date, price_now, average;
    Model model;
    BarChart barChart;
    Calendar cal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.setTitle("");
        date = (TextView) findViewById(R.id.textView7);
        price_now = (TextView) findViewById(R.id.textView5);
        average = (TextView) findViewById(R.id.textView6);

        cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
        String formatted = format1.format(cal.getTime());
        cal.add(Calendar.DATE, -1);
        String formatted2 = format1.format(cal.getTime());
        cal.add(Calendar.DATE, 1);


        date.setText(formatted2 + " - " + formatted);
        //подключаю экземпляр класса DataBase к экземпляру класса Model
        model = new Model(new DataBase());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar_button = (ImageButton) toolbar.findViewById(R.id.toolbar_button);
        button_back = (ImageButton) findViewById(R.id.imageButton2);
        button_forward = (ImageButton) findViewById(R.id.imageButton3);
        toolbar_button.setOnClickListener(this);
        button_back.setOnClickListener(this);
        button_forward.setOnClickListener(this);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        customizeToolbar(toolbar);



        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        barChart = (BarChart) findViewById(R.id.chart);

        List<BarEntry> entries = new ArrayList<>();
        int[] result = model.getDataFromDay(null);

        int average_cost = 0;
        for (int i = 0; i < result.length; i++){
            entries.add(new BarEntry(i, result[i]));
            average_cost += result[i];
        }
        average_cost /= result.length;
        average.setText("" + average_cost + " c/kWh");
        price_now.setText("" + result[10] + " c/kWh");

        BarDataSet dataset = new BarDataSet(entries, "BarDataSet");
        dataset.setColor(Color.parseColor("#FF8000"));
        //barChart.setDrawValueAboveBar(true);


        BarData data = new BarData(dataset);
                barChart.setData(data); // set the data and list of lables into chart<br />


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void customizeToolbar(Toolbar toolbar) {



        toolbar.setNavigationIcon(null);
        //toolbar.setTitle("Title");
        //toolbar.setSubtitle("Sub");
        //toolbar.setLogo(R.drawable.logo);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.toolbar_button:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.imageButton2:
                // меняю дату вперед
                SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
                String formatted = format1.format(cal.getTime());
                cal.add(Calendar.DATE, 1);
                String formatted2 = format1.format(cal.getTime());
                date.setText(formatted + " - " + formatted2);

                fillChart(model.getDataFromDay(null), barChart);

                break;
            case R.id.imageButton3:
                //меняю дату назад
                SimpleDateFormat format2 = new SimpleDateFormat("dd.MM.yyyy");
                cal.add(Calendar.DATE, -2);
                String formatted3 = format2.format(cal.getTime());
                cal.add(Calendar.DATE, 1);
                String formatted4 = format2.format(cal.getTime());
                date.setText(formatted3 + " - " + formatted4);
                fillChart(model.getDataFromDay(null), barChart);
                break;
        }

    }

    private void fillChart(int[] date, BarChart barChart){
        List<BarEntry> entries = new ArrayList<>();
        int average_cost = 0;
        for (int i = 0; i < date.length; i++){
            entries.add(new BarEntry(i, date[i]));
            average_cost += date[i];
        }
        average_cost /= date.length;
        average.setText("" + average_cost + " c/kWh");
        price_now.setText("" + date[10] + " c/kWh");

        BarDataSet dataset = new BarDataSet(entries, "BarDataSet");
        dataset.setColor(Color.parseColor("#FF8000"));
        //barChart.setDrawValueAboveBar(true);


        BarData data = new BarData(dataset);
        barChart.setData(data);
        barChart.invalidate();
    }

}
